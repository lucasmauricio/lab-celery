# Python Celery Lab

This is an experimental project to develop some reactive programs using the Celery library.

By the way, I suggest to use [VirtualEnv](https://virtualenv.pypa.io/) for running it without changing your system configuration.
The file `requirements.txt` describes all projects' dependencies.

https://fernandofreitasalves.com/tarefas-assincronas-com-django-e-celery/


