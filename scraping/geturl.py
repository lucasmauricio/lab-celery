"""
A sample project to check how to use the Celery library.

In this program, we request an URL and get the status code of this request.

To run it:
celery -A tasks worker -l info


Reference: 
- sample at https://www.youtube.com/watch?v=-ISgjBQDnhw
"""
import requests
from celery import Celery

app = Celery()
app.config_from_object('config')

@app.task
def fetch_url(url):
    r = requests.get(url)
    return r.status_code
