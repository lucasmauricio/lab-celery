"""
A sample project to check how to use the Celery library.

In this program, we just sum two number.

To run it:
celery -A tasks worker -l info


Reference:
- samples at http://docs.celeryproject.org/en/latest/userguide/application.html
- https://www.youtube.com/watch?v=68QWZU_gCDA
"""

from celery import Celery

app = Celery('tasks', broker='pyamqp://guest@localhost//')

@app.task
def add2(x, y):
    return x + y +1
